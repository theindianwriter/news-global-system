import React from 'react';
import './componentStyle.css';

class Button extends React.Component{
    render(){
        return(
            <button onClick={this.props.click}>{this.props.data}</button>
        );
    }
}

export default Button;