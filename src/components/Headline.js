import React from 'react';
import './componentStyle.css';

class Headline extends React.Component{
    render(){
        return(
            <p>{this.props.description} <span className="newsSource">{this.props.name}</span>
                <br></br>
                <a href={this.props.forMore}>for more click...</a>
            </p>   
        );
    }
}

export default Headline;