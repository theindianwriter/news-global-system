import React, { Component } from 'react';
import Button from './components/Button.js';
import Image from './components/Image.js';
import Headline from './components/Headline.js';
import Clock from './components/Clock.js';
import './App.css';



const countries=["India","Britain","USA","Russia","France"];
class App extends Component {
	constructor(props){
		super(props);
		this.state={
			items : [],
			isLoaded : false,
			selected: null
		};
		
	}
	generateCountryCode(country){
		if(country === "India")
			return "in";
		else if(country === "Britain")
			return "gb";
		else if(country === "USA")
			return "us";
		else if(country === "Russia")
			return "ru";
		else return "fr";
				
	}

	getNewsBlock=()=>{
		return(
			this.state.isLoaded ? ((this.state.items.articles).map(item=>(
			<div key={item.url} className="newsBox">
			<Image url={item.urlToImage} />
			<Headline description={item.description} name={item.source.name} forMore={item.url}/>
			</div>
		  ))) : (<div></div>)
		);
	}

	getButtonsBlock=()=>{
		return(
			countries.map(country => (
			<Button key={country} click={()=>this.getHeadlines(country)} data={country}>
			</Button>
			))
		);
	}

	getLogo=()=>{
		return(
			<div className="App-logo">
					<Image url="https://media.giphy.com/media/d0jolDRzq28F2/giphy.gif" />
			</div>
		);
	}

	getHeadlines(country){
		let countryCode = this.generateCountryCode(country);
		var url = `https://newsapi.org/v2/top-headlines?source=google-news&country=${countryCode}&apiKey=a6dfe55c34a84a5981ba3e4d494f4c62`;
		var req = new Request(url);
		fetch(req)
		.then(response=> response.json())
		.then((json) =>{
			this.setState(()=>{
				return ({isLoaded: true,items : json,selected: country});
			});
		});
			
	}
	render() {
		return (
			<div className="App">
					<div className="Header">
								{
									this.getLogo()
								}
						   	<Clock />
						  	<h1>Choose a country and read the top news headlines </h1>
							    {
								    this.getButtonsBlock()
							    }
				   </div>
				  <div className="selectedBox">
					 {
						this.state.selected ? this.state.selected : <h2></h2>
					 }
				  </div>
				  <div>
							{
						   this.getNewsBlock()
					  	}
				 </div>
			</div>
				
		);
	}
}

export default App;
